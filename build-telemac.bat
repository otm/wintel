@echo off

rem Local variables only
setlocal

rem ****************************************
rem Dependency versions must be defined here
rem ****************************************
set hdf5=hdf5-1.10.9
set med=med-4.1.1
set metis=metis-5.1.0
set msmpi=msmpi-10.1.3
set openblas=openblas-0.3.25
set scalapack=scalapack-2.1.0
set mumps_version=5.6.2
set mumps=mumps-%mumps_version%
set aed2=libaed2-1.2.0
set gotm=gotm-2019-06-14-opentelemac

set python_version=3.11.9
set python_launcher_version=3.11.9
set python=python-%python_version%
set python_requirements=python_requirements.txt
set pip_version=24.0
set setuptools_version=65.5.0
set geo_wheels_version=v2024.9.22
set gdal=GDAL-3.9.2-cp311-cp311-win_amd64.whl
set fiona=fiona-1.10.1-cp311-cp311-win_amd64.whl
set rasterio=rasterio-1.3.11-cp311-cp311-win_amd64.whl
set shapely=shapely-2.0.6-cp311-cp311-win_amd64.whl

rem ************************************************************************
rem Parse input arguments to determine where and how TELEMAC should be built
rem ************************************************************************

rem Check that the installation path has been provided
set install_path=%~1
if "%install_path:~0,1%" == "/" set install_path=
if "%install_path%" == "" (
  echo The syntax is:
  echo:
  echo %~n0 path [/comp[[:]compiler]] [/v[[:]version]]
  echo:
  echo Description:
  echo          path: Path where TELEMAC will be installed.
  echo:
  echo          /c    Compiler to use, either mingw (default^) or intel.
  echo          /v    Version to build, either V8P4, V8P5 or main (default^)
  echo          /e    Path to the externals (Python, MED, MUMPS...^). If not set,
  echo                the externals will be installed under a TELEMAC subdirectory.
  exit /b 0
)

rem Ensure that the installation path is absolute
set install_path=%~f1

rem Parse optional arguments and fallback to default values if needed
for /f "tokens=1,* delims= " %%a in ("%*") do set args=%%b
call :arg_parser %args%
if not defined arg_c set arg_c=mingw
if not defined arg_v set arg_v=main
if not defined arg_e set arg_e=%install_path%\external

set external_path=%arg_e%

rem ************************************
rem Check that the compiler is available
rem ************************************
if /i %arg_c% == mingw (
  set compiler=mingw
  goto find_mingw
)

if /i %arg_c% == intel (
  set compiler=intel
  goto find_intel
)

echo Unsupported compiler: %arg_c%
exit /b 0

:find_mingw
rem Try to find Mingw in the current PATH
set mingw_path=
for /f "delims=" %%a in ('"where mingw32-make 2>nul"') do set mingw_path=%%~dpa
rem If not found, add the usual Mingw location to PATH
if not defined mingw_path set path=C:\mingw64\bin;%path%
for /f "delims=" %%a in ('"where mingw32-make 2>nul"') do set mingw_path=%%~dpa
if not defined mingw_path (
  echo Error: Mingw not found. Please ensure that Mingw bin folder is in your PATH.
  exit /b 0
)
rem Remove the trailing backslash
set mingw_path=%mingw_path:~0,-1%
goto compiler_set

:find_intel
rem TODO

:compiler_set

rem *********************************************************************
rem Check that the version is supported and set Python requirements
rem accordingly, including versions of the unofficial geopospatial wheels
rem provided by Christoph Gohlke
rem *********************************************************************
set python_requirements=python_requirements.txt
if /i %arg_v% == v8p4 (
  set version=v8p4
  set branch=%version%

  set openblas=openblas-0.3.25
  set scalapack=scalapack-2.1.0
  set mumps_version=5.2.1
  set mumps=mumps-%mumps_version%

  set python_version=3.10.11
  set python=python-3.10.11
  set python_requirements=python_requirements_v8p4.txt
  set pip_version=22.2.2
  set setuptools_version=63.2.0
  set geo_wheels_version=v2023.11.3
  set gdal=GDAL-3.7.3-cp310-cp310-win_amd64.whl
  set fiona=fiona-1.9.5-cp310-cp310-win_amd64.whl
  set rasterio=rasterio-1.3.9-cp310-cp310-win_amd64.whl
  set shapely=shapely-2.0.2-cp310-cp310-win_amd64.whl
)
if /i %arg_v% == v8p5 (
  set version=v8p5
  set branch=%version%

  set openblas=openblas-0.3.25
  set scalapack=scalapack-2.1.0
  set mumps_version=5.2.1
  set mumps=mumps-%mumps_version%

  set python_version=3.11.9
  set python_launcher_version=3.11.9
  set python_requirements=python_requirements_v8p5.txt
  set pip_version=24.0
  set setuptools_version=65.5.0
  set geo_wheels_version=v2023.11.3
  set gdal=GDAL-3.7.3-cp311-cp311-win_amd64.whl
  set fiona=fiona-1.9.5-cp311-cp311-win_amd64.whl
  set rasterio=rasterio-1.3.9-cp311-cp311-win_amd64.whl
  set shapely=shapely-2.0.2-cp311-cp311-win_amd64.whl
)
if /i %arg_v% == 9.0 (
  set version=9.0
  set branch=v%version%

  set openblas=openblas-0.3.28
  set scalapack=scalapack-2.2.1
  set mumps_version=5.6.2
  set mumps=mumps-%mumps_version%

  set python_version=3.11.9
  set python_launcher_version=3.11.9
  set python_requirements=python_requirements_9.0.txt
  set pip_version=24.0
  set setuptools_version=65.5.0
  set geo_wheels_version=v2024.9.22
  set gdal=GDAL-3.9.2-cp311-cp311-win_amd64.whl
  set fiona=fiona-1.10.1-cp311-cp311-win_amd64.whl
  set rasterio=rasterio-1.3.11-cp311-cp311-win_amd64.whl
  set shapely=shapely-2.0.6-cp311-cp311-win_amd64.whl
)
if /i %arg_v% == main (
  set version=main
  set branch=main
)
if not defined version (
  echo Unsupported TELEMAC version: %arg_v%
  exit /b 0
)

rem **************************************
rem Check that all Git and CMake available
rem **************************************

rem Check for the presence of Git
set git_path=
for /f "delims=" %%a in ('"where git 2>nul"') do set git_path=%%a
if not defined git_path (
  echo Error: Git not found. Please ensure that Git is in your PATH.
  exit /b 0
)

rem Check for the presence of CMake
set cmake_path=
for /f "delims=,tokens=*" %%a in ('"where cmake 2>nul"') do set cmake_path=%%a
if not defined cmake_path (
  echo Error: CMake not found. Please ensure that CMake is in your PATH.
  exit /b 0
)

rem *************
rem Clone Telemac
rem *************
set clone=n
if exist %install_path% (
  set /p clone=""%install_path%" already exists, do yo want to remove it (y[n])? "
) else (
  set clone=y
)
if /i "%clone%" equ "y" (
    rd /s/q %install_path% 2>nul
    git clone -b %branch% https://gitlab.pam-retd.fr/otm/telemac-mascaret.git %install_path% || exit /b 0
)

rem **********************
rem Build the dependencies
rem **********************
pushd %~dp0deps

if %compiler% == intel (
  goto intel_build
)

call :build_hdf5 || goto exit
call :build_med || goto exit
call :build_metis || goto exit
call :build_msmpi || goto exit
call :build_openblas || goto exit
call :build_scalapack || goto exit
call :build_mumps || goto exit
call :build_aed2 || goto exit
call :build_gotm || goto exit
call :install_python || goto exit
call :install_python_packages || goto exit
popd
goto build_telemac

:intel_build
call :build_hdf5 || goto exit
call :build_med || goto exit
call :build_metis || goto exit
rem call :build_mumps || goto exit
rem call :build_aed2 || goto exit
rem call :buid_gotm || goto exit
call :install_python || goto exit
call :install_python_packages || goto exit
popd

rem *************
rem Build TELEMAC
rem *************
:build_telemac
pushd %install_path%

rem Add Python to PATH
set path=%external_path%\%python%\Scripts;%external_path%\%python%;%path%

rem Copy systel.cfg and create the pysource batches
pushd configs
xcopy /y %~dp0config\systel.cfg .\
call :write_pysource_bat gnu.static
call :write_pysource_bat gnu.dynamic
call :write_pysource_bat gnu.static.debug
call :write_pysource_bat gnu.dynamic.debug
popd

rem Build TELEMAC for gnu.static
call configs\pysource.gnu.static.bat
call compile_telemac.py

if %errorlevel% == 0 (
  rem Copy DLLs required by the API and Hermes Python modules
  xcopy /y %mingw_path%\libgcc_s_seh-1.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %mingw_path%\libgfortran-5.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %mingw_path%\libquadmath-0.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %mingw_path%\libstdc++-6.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %mingw_path%\libwinpthread-1.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %external_path%\%hdf5%\bin\libhdf5.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %external_path%\%med%\bin\libmedC.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %external_path%\%med%\bin\libmedfwrap.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %external_path%\%msmpi%\bin\msmpi.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %external_path%\%gotm%\bin\libturbulence.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
  xcopy /y %external_path%\%gotm%\bin\libutil.dll %install_path%\builds\gnu.static\wrap_api\lib\ >nul
)

:exit
endlocal
popd
color 7
exit /b 0

rem ****
rem HDF5
rem ****
:build_hdf5
call :download %hdf5% "https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.9/src/hdf5-1.10.9.zip" || exit /b 1
set cmake_config_options=-DONLY_SHARED_LIBS:BOOL=ON -DHDF5_GENERATE_HEADERS:BOOL=OFF -DBUILD_TESTING:BOOL=OFF -DHDF5_BUILD_EXAMPLES:BOOL=OFF -DHDF5_BUILD_TOOLS:BOOL=OFF -DHDF5_BUILD_UTILS:BOOL=OFF -DHDF5_BUILD_HL_LIB:BOOL=OFF -DHDF5_BUILD_HL_TOOLS:BOOL=OFF
call :cmake_build %hdf5% || exit /b 1
exit /b 0

rem ***
rem MED
rem ***
:build_med
call :download %med% "https://gitlab.pam-retd.fr/otm/telemac-mascaret/-/package_files/443/download" || exit /b 1
set cmake_config_options=-DHDF5_ROOT_DIR:PATH=%external_path%/%hdf5% -DMEDFILE_BUILD_TESTS:BOOL=OFF -DMEDFILE_INSTALL_DOC:BOOL=OFF -DCMAKE_C_FLAGS="-Wno-error=implicit-function-declaration"
if %compiler% == intel (
  rem MED libraries cannot be built as shared libraries with Visual Studio generator as they
  rem miss the proper __declspec(dllexport) declarations on each class and function
  set cmake_config_options=%cmake_config_options% -DMEDFILE_BUILD_SHARED_LIBS:BOOL=OFF -DMEDFILE_BUILD_STATIC_LIBS:BOOL=ON
)
call :cmake_build %med% || exit /b 1
exit /b 0

rem *****
rem METIS
rem *****
:build_metis
call :download %metis% "https://github.com/scivision/METIS/archive/refs/tags/v5.1.0.3.tar.gz" || exit /b 1
set cmake_config_options=-DBUILD_SHARED_LIBS:BOOL=OFF
call :cmake_build %metis% || exit /b 1
exit /b 0

rem ******
rem MS-MPI
rem ******
:build_msmpi
echo Building %msmpi%...
set carch=x86_64
pushd %msmpi%
cd sdk
rd /s/q build-%carch% 2>nul
mkdir build-%carch%
cd build-%carch%
mkdir bin include lib
rem Generate import library
dlltool -k -d ..\msmpi.def.%carch% -l lib\libmsmpi.a || popd && exit /b 1
rem Headers
copy ..\mpi.h include\
copy ..\mpif.h include\
copy ..\mpi.f90 include\
copy ..\mpifptr.h.%carch% include\mpifptr.h
rem Build compiler wrappers
set mpic=..\mpi.c
set cflags=-s -O2 -DNDEBUG
gcc %cflags% -o bin\mpicc.exe -DCC %mpic% || popd && exit /b 1
gcc %cflags% -o bin\mpicxx.exe -DCXX %mpic% || popd && exit /b 1
copy bin\mpicxx.exe "bin\mpic++.exe"
gcc %cflags% -o bin\mpifort.exe -DFC %mpic% || popd && exit /b 1
copy bin\mpifort.exe bin\mpif77.exe
copy bin\mpifort.exe bin\mpif90.exe
rem Build FORTRAN90 MPI modules
bin\mpifort -c -Jinclude ..\mpi.f90 || popd && exit /b 1
del mpi.o
cd ..\..
rem Installation
xcopy /e/y bin %external_path%\%msmpi%\bin\
xcopy /e/y license %external_path%\%msmpi%\license\
xcopy /e/y sdk\build-x86_64 %external_path%\%msmpi%\
popd
exit /b 0

rem ********
rem OpenBLAS
rem ********
:build_openblas
call :download %openblas% "https://github.com/xianyi/OpenBLAS/archive/refs/tags/v0.3.24.tar.gz" || exit /b 1
set cmake_config_options=-DBUILD_TESTING=OFF
call :cmake_build %openblas% || exit /b 1
exit /b 0

rem *********
rem ScaLAPACK
rem *********
:build_scalapack
call :download %scalapack% "https://github.com/Reference-ScaLAPACK/scalapack/archive/refs/tags/v2.1.0.zip" || exit /b 1
rem We need to use a foward slash for lapack and blas libraries paths since CMake CheckFortranFunctionExists is bugged when using a backward slash
set ext_path=%external_path:\=/%
set cmake_config_options=-DMPI_BASE_DIR:PATH=%ext_path%/%msmpi% -DLAPACK_LIBRARIES:STRING=%ext_path%/%openblas%/lib/libopenblas.a -DBLAS_LIBRARIES:STRING=%ext_path%/%openblas%/lib/libopenblas.a -DBUILD_TESTING:BOOL=OFF -DCMAKE_C_FLAGS="-Wno-error=implicit-function-declaration -Wno-implicit-int"
call :cmake_build %scalapack% || exit /b 1
exit /b 0

rem *****
rem MUMPS
rem *****
:build_mumps
call :download %mumps% "https://github.com/scivision/mumps/archive/refs/tags/v5.7.3.1.tar.gz" || exit /b 1
set cmake_config_options=-DMUMPS_UPSTREAM_VERSION=%mumps_version% -DBLAS_LIBRARY=%external_path%\%openblas%\lib\libopenblas.a -DLAPACK_LIBRARY=%external_path%\%openblas%\lib\libopenblas.a -DSCALAPACK_ROOT:PATH=%external_path%\%scalapack%
if %compiler% == mingw (
  set cmake_config_options=%cmake_config_options% -DMPI_ROOT:PATH=%external_path%\%msmpi%
) else (
  if not defined MKLROOT (
    rem Set Intel MKL environment
    call "%ONEAPI_ROOT%mkl\latest\env\vars.bat"
  )
  if not defined I_MPI_ROOT (
    rem Set Intel MPI environment
    call "%I_MPI_ONEAPI_ROOT%\env\vars.bat"
  )
  set cmake_config_options=%cmake_config_options% -DMPI_ROOT:PATH="%I_MPI_ROOT%"
)
call :cmake_build %mumps% || exit /b 1
exit /b 0

rem ****
rem AED2
rem ****
:build_aed2
call :download %aed2% "https://gitlab.pam-retd.fr/otm/telemac-mascaret/-/package_files/444/download" || exit /b 1
echo Building libaed2...
set install_dir=%external_path%\%aed2%
rd /s/q %install_dir% 2>nul
pushd %aed2%
mingw32-make || goto build_aed2_error
mkdir %install_dir%
xcopy /e/y include %install_dir%\include\
xcopy /y .\*.mod %install_dir%\include\
xcopy /y .\*.a %install_dir%\lib\
popd
exit /b 0

:build_aed2_error
popd
exit /b 1

rem ****
rem GOTM
rem ****
:build_gotm
call :download %gotm% "https://gitlab.pam-retd.fr/otm/telemac-mascaret/-/package_files/445/download" || exit /b 1
set cmake_config_options=-DBUILD_SHARED_LIBS:BOOL=ON -DGOTM_USE_NetCDF:BOOL=OFF -DGOTM_EMBED_VERSION:BOOL=OFF
call :cmake_build %gotm% || exit /b 1
exit /b 0

rem ******
rem Python
rem ******
:install_python
if exist %external_path%\%python% (
  exit /b 0
)

pushd %external_path%

echo Downloading and extracting Python %python_version%...
curl -L https://www.python.org/ftp/python/%python_version%/%python%-embed-amd64.zip -o python.zip || goto install_python_error
mkdir %python% 2>nul
tar -xvf python.zip -C %python% || goto install_python_error
del python.zip

rem Download the Python launcher and install it in the current user's programs. Any launcher
rem installed in %LOCALAPPDATA%\Programs\Python\Launcher has priority over the launcher
rem installed in C:\Windows.
rem What's more, the launcher will not update an already installed and more recent version.
curl -OJL https://www.python.org/ftp/python/%python_launcher_version%/win32/launcher.msi
msiexec /i launcher.msi /quiet
del launcher.msi
rem Ensure .py files are properly opened with the Python Launcher
call :restore_py_launcher

rem Download Python dev.msi package to get Python include folder, required by F2PY
curl -OJL https://www.python.org/ftp/python/%python_version%/amd64/dev.msi
msiexec /a dev.msi /qb TARGETDIR=%external_path%\%python% /quiet
del dev.msi
cd %python%
rd /s/q libs
del dev.msi

rem Update the _pth file to include TELEMAC scripts and API folders
for /f "tokens=*" %%g in ('dir python*._pth /b') do (set pth_file=%%g)
for /f "tokens=*" %%g in ('dir python*.zip /b') do (set zip_file=%%g)
echo %zip_file%> %pth_file%
echo .>> %pth_file%
echo Lib\site-packages>> %pth_file%
echo ..\..\scripts\python3>> %pth_file%
echo ..\..\builds\gnu.static\wrap_api\lib>> %pth_file%
echo import site>> %pth_file%

rem Copy python.exe to python3.exe to ensure the proper execution of TELEMAC
rem scripts having the shebang form #!/usr/bin/env python3
copy /y python.exe python3.exe

rem Create the import library for the Python dll, required by F2PY
mkdir libs 2>nul
for /f "tokens=1,2 delims=." %%a in ("%python_version%") do set python_lib=python%%a%%b
gendef - %python_lib%.dll > tmp.def || goto install_python_error
dlltool --dllname %python_lib%.dll --def tmp.def --output-lib libs\lib%python_lib%.a || goto install_python_error
del tmp.def

rem Download and install pip and install setuptools
curl -OJ https://bootstrap.pypa.io/get-pip.py || goto install_python_error
%cd%\python get-pip.py "pip==%pip_version%" --no-setuptools --no-wheel --no-warn-script-location || goto install_python_error
del get-pip.py
%cd%\python -m pip install setuptools==%setuptools_version%

popd
exit /b 0

:install_python_error
rd /s/q %python%
del tmp.def 2>nul
del *.zip 2>nul
del *.tgz 2>nul
popd
exit /b 1

rem ***************
rem Python packages
rem ***************
:install_python_packages
rem Install all Python requirements for TELEMAC-MASCARET
echo Installing Python requirements...

set path=%external_path%\%python%\Scripts;%external_path%\%python%;%path%

mkdir %~dp0wheels 2>nul
pushd %~dp0wheels
echo https://github.com/cgohlke/geospatial-wheels/releases/download/%geo_wheels_version%/%gdal% 
curl -OL https://github.com/cgohlke/geospatial-wheels/releases/download/%geo_wheels_version%/%gdal% || goto install_python_packages_error
curl -OL https://github.com/cgohlke/geospatial-wheels/releases/download/%geo_wheels_version%/%fiona% || goto install_python_packages_error
curl -OL https://github.com/cgohlke/geospatial-wheels/releases/download/%geo_wheels_version%/%rasterio% || goto install_python_packages_error
curl -OL https://github.com/cgohlke/geospatial-wheels/releases/download/%geo_wheels_version%/%shapely% || goto install_python_packages_error
popd
pushd %~dp0
pip install -r %python_requirements% --no-warn-script-location || goto install_python_packages_error

rem Starting with Python 3.8, mpi4py requires MPI DLLs to be present in the module directory: copy them there
xcopy /y %external_path%\%msmpi%\bin\msmpi*.dll %external_path%\%python%\Lib\site-packages\mpi4py\ >nul

popd
exit /b 0

:install_python_packages_error
popd
exit /b 1

rem ***********************************************
rem Parse commandline arguments into sane variables
rem ***********************************************
:arg_parser
rem Loop until two consecutive empty args
if "%~1%~2" equ "" exit /b 0

set "arg1=%~1" 
set "arg2=%~2"
shift

rem Allow either / or -
set "tst1=%arg1:-=/%"
if "%arg1%" neq "" (
  set "tst1=%tst1:~0,1%"
) else (
  set "tst1="
)

set "tst2=%arg2:-=/%"
if "%arg2%" neq "" (
  set "tst2=%tst2:~0,1%"
) else (
  set "tst2="
)

rem Capture assignments
if "%tst1%" equ "/"  if "%tst2%" neq "/" if "%tst2%" neq "" (
  set "arg_%arg1:~1%=%arg2%"
  goto arg_parser
)

rem Capture flags
if "%tst1%" equ "/" (
    set "arg_%arg1:~1%=1"
    goto arg_parser
)
goto arg_parser

rem **************************************
rem Download a library if not already done
rem **************************************
:download
set lib=%~1
set url="%~2"

if exist %lib% (
  exit /b 0
)

rem Download and extract
echo Downloading and extracting %lib%...
set output=%lib%_arch
curl -L %url% -o %output% || exit /b 1
mkdir %lib%
tar -xvzf %output% --strip-components=1 -C %lib% || exit /b 1
del %output%
rem Apply patch if needed
if exist %~dp0patches\%lib%.patch (
  echo Patching %lib%...
  pushd %lib%
  git apply --ignore-space-change %~dp0patches\%lib%.patch || popd && exit /b 1
  popd
)

exit /b 0

rem ********************
rem CMake build function
rem ********************
:cmake_build
set lib=%~1

rem Set the number of build processes
set /a build_proc=%number_of_processors%/2

if %compiler% == mingw (
  set cmake_config_options=%cmake_config_options% -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release
  set cmake_build_options=-j %build_proc%
) else (
  if %compiler% == intel (
    set cmake_config_options=%cmake_config_options% -G "Visual Studio 16 2019" -A x64 -T "Intel C++ Compiler 19.2" -DCMAKE_CONFIGURATION_TYPES:STRING=Release
    set cmake_build_options=--config Release -j %build_proc%
  ) else (
    echo Unknown compiler: %compiler%.
    exit /b 1
  )
)

echo Building %lib%...

pushd %lib%
mkdir build-%compiler% 2>nul
cd build-%compiler%
cmake %cmake_config_options% -DCMAKE_INSTALL_PREFIX:PATH=%external_path%/%lib% .. || goto cmake_build_error
cmake --build . %cmake_build_options% || goto cmake_build_error
cmake --install . || goto cmake_build_error
popd
color 7
exit /b 0

:cmake_build_error
popd
exit /b 1


rem ******************************************************
rem Write a pysource batch for a given build configuration
rem ******************************************************
:write_pysource_bat
set file_name=pysource.%1.bat
echo @echo off> %file_name%
echo set PROMPT=(telemac %version%) %%PROMPT%%>> %file_name%
echo rem Path to TELEMAC root directory>> %file_name%
echo for %%%%a in ("%%~dp0.") do set HOMETEL=%%%%~dpa>> %file_name%
echo set HOMETEL=%%HOMETEL:~0,-1%%>> %file_name%
echo rem Path to dependencies>> %file_name%
echo set EXTERNAL=%external_path%>> %file_name%
echo rem Path to this file>> %file_name%
echo set SOURCEFILE=%%~dp0%%~nx0>> %file_name%
echo rem Configuration file>> %file_name%
echo set SYSTELCFG=%%~dp0systel.cfg>> %file_name%
echo rem Name of the configuration to use>> %file_name%
echo set USETELCFG=%1>> %file_name%
echo:>> %file_name%
echo rem Add Python to PATH and unset PYTHONHOME>> %file_name%
echo set PATH=%%EXTERNAL%%\%python%\Scripts;%%EXTERNAL%%\%python%;%%PATH%%>> %file_name%
echo set PYTHONHOME=>> %file_name%
echo:>> %file_name%
echo rem Add Mingw to PATH>> %file_name%
echo set PATH=%mingw_path%;%%PATH%%>> %file_name%
echo:>> %file_name%
echo rem Add TELEMAC binaries directory to PATH>> %file_name%
echo set PATH=%%HOMETEL%%\builds\%%USETELCFG%%\lib;%%PATH%%>> %file_name%
echo rem Add TELEMAC Python scripts folder to PATH>> %file_name%
echo set PATH=%%HOMETEL%%\scripts\python3;%%PATH%%>> %file_name%
echo:>> %file_name%
echo rem Paths to external libraries>> %file_name%
echo rem HDF5>> %file_name%
echo set HDF5HOME=%%EXTERNAL%%\%hdf5%>> %file_name%
echo set PATH=%%HDF5HOME%%\bin;%%PATH%%>> %file_name%
echo rem MED>> %file_name%
echo set MEDHOME=%%EXTERNAL%%\%med%>> %file_name%
echo set PATH=%%MEDHOME%%\bin;%%PATH%%>> %file_name%
echo rem METIS>> %file_name%
echo set METISHOME=%%EXTERNAL%%\%metis%>> %file_name%
echo set PATH=%%METISHOME%%\bin;%%PATH%%>> %file_name%
echo rem MSPMPI>> %file_name%
echo set MPIHOME=%%EXTERNAL%%\%msmpi%>> %file_name%
echo set PATH=%%MPIHOME%%\bin;%%PATH%%>> %file_name%
echo rem OPENBLAS, SCALAPACK and MUMPS>> %file_name%
echo set OPENBLASHOME=%%EXTERNAL%%\%openblas%>> %file_name%
echo set SCALAPACKHOME=%%EXTERNAL%%\%scalapack%>> %file_name%
echo set MUMPSHOME=%%EXTERNAL%%\%mumps%>> %file_name%
echo rem AED>> %file_name%
echo set AEDHOME=%%EXTERNAL%%\%aed2%>> %file_name%
echo rem GOTM>> %file_name%
echo set GOTMHOME=%%EXTERNAL%%\%gotm%>> %file_name%
echo set PATH=%%GOTMHOME%%\bin;%%PATH%%>> %file_name%

rem *******************************************************************************
rem Generate a .reg file to reset Python files association with the Python Launcher
rem *******************************************************************************
:restore_py_launcher
set file_name=reset_py.reg
echo Windows Registry Editor Version 5.00> %file_name%
echo:>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.py]>> %file_name%
echo:>> %file_name%
echo [-HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.py\OpenWithList]>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.py\OpenWithList]>> %file_name%
echo:>> %file_name%
echo [-HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.py\OpenWithProgids]>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.py\OpenWithProgids]>> %file_name%
echo "Python.File"=hex(0):>> %file_name%
echo:>> %file_name%
echo [-HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.py\UserChoice]>> %file_name%
echo:>> %file_name%
echo [-HKEY_CURRENT_USER\SOFTWARE\Classes\.py]>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\.py]>> %file_name%
echo @="Python.File">> %file_name%
echo "Content Type"="text/x-python">> %file_name%
echo:>> %file_name%
echo [-HKEY_CURRENT_USER\SOFTWARE\Classes\py_auto_file]>> %file_name%
echo:>> %file_name%
echo [-HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File]>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File]>> %file_name%
echo @="Python File">> %file_name%
echo:>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File\DefaultIcon]>> %file_name%
echo @="\"C:\\Users\\%username%\\AppData\\Local\\Programs\\Python\\Launcher\\py.exe\",1">> %file_name%
echo:>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File\shell]>> %file_name%
echo:>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File\shell\open]>> %file_name%
echo:>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File\shell\open\command]>> %file_name%
echo @="\"C:\\Users\\%username%\\AppData\\Local\\Programs\\Python\\Launcher\\py.exe\" \"%%L\" %%*">> %file_name%
echo:>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File\shellex]>> %file_name%
echo:>> %file_name%
echo [HKEY_CURRENT_USER\SOFTWARE\Classes\Python.File\shellex\DropHandler]>> %file_name%
echo @="{BEA218D2-6950-497B-9434-61683EC065FE}">> %file_name%
reg import reset_py.reg
del reset_py.reg
