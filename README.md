# Building TELEMAC-MASCARET on Windows

This repository provides a batch script and configuration files to build TELEMAC-MASCARET on Windows using Mingw-w64.

The build includes the following dependencies:
- MED (requires HDF5): add support for the MED file format.
- MPI: enable runtime parallelism, using the latest implementation of Microsoft MPI. Also requires:
    - METIS: used for partioning the mesh during a parallel run.
- MUMPS: parallel direct solver which is only used by ARTEMIS. Also requires:
    - MPI
    - OpenBLAS (includes LAPACK)
    - ScaLAPACK
- AED2: activate more processes in WAQTEL.
- GOTM: allow the use of the General Ocean Turbulence Model in TELEMAC-3D.

## Prerequisites:

- **Git**: download it from [gitforwindows.org](https://gitforwindows.org/) and install it with the LFS extension.
- **CMake**: install the latest 64 bits version available from [cmake.org](https://cmake.org), and ensure that CMake
`bin` directory is added to your `PATH` environment variable.<br>
Note: to install CMake without administrator rights, you need to download and extract the zip file and set your `PATH`
manually.
- **Mingw-w64**: download the 12.3 version from [winlibs](https://github.com/brechtsanders/winlibs_mingw/releases/download/12.3.0-16.0.4-11.0.0-msvcrt-r1/winlibs-x86_64-posix-seh-gcc-12.3.0-mingw-w64msvcrt-11.0.0-r1.zip) and extract it to `C:\`.<br>
Note: if you don't install Mingw-w64 in `C:\`, you will need to add its `bin` directory to your `PATH`.

You don't need to install Python, as a version of its embeddable package will be downloaded and used by the batch script. 

## Optional: proxy configuration

If your computer is behind a proxy, you must also set the proxy environment variables, as in the example below:
```
C:\>set http_proxy=http://vip-users.proxy.edf.fr:3131
C:\>set https_proxy=http://vip-users.proxy.edf.fr:3131
```

## Building and installing TELEMAC-MASCARET

Once all software prerequisites have been installed and the proxy environment variables have been defined if necessary, TELEMAC-MASCARET can be built using the provided batch file. The only mandatory argument is the installation path, e.g.,
to install TELEMAC-MASCARET in `C:\telemac`:

```
C:\wintel>build-telemac C:\telemac
```

This will **download** , **build** and **install** a full setup of the current development version of TELEMAC-MASCARET (i.e. the `main` branch) in the provided folder, which is `C:\telemac` in this example.

To build a specific version, you need to specify it using the `/v` switch, e.g:

```
C:\wintel>build-telemac C:\telemac /v V8P5
```

The supported stable versions are V8P4 and V8P5.

If the compilation went fine, the last message you will get should be `My work is done`.

By default, all TELEMAC external dependencies, including Python, will be downloaded, built if needed, and installed under a TELEMAC subfolder called "external". However, you can specify the location of the externals directory using the `/e` switch, e.g:

```
C:\wintel>build-telemac C:\telemac /e C:\telemac-deps
```

This allows you to reuse the same externals for several local installations of TELEMAC.

